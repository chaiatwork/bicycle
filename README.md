# Document Version
4.0 (PM Sat 17th May 2021)


# Date
10:00 AM  
Saturday 26th June 2021  
leaving from Jeff's Shed, opposite Crown Casino.


# Description
* A few of us are thinking of riding to **Burnside Heights** (wherever that is)
    to sample some fine **Filipino** cuisine.
* **Motivated** by this  
    https://www.sbs.com.au/language/english/filipino-cafe-bike-shop-in-melbourne-s-west-becomes-cyclist-pit-stop-and-local-favourite
* The ride back **detours** via Footscray to possibly check out a **canolli** place  
    https://www.tcavallaroandsons.com.au  
    The coffee there is pretty decent too.
* ~~Feel free to **invite** anyone you think might be interested (within reason)~~  
    Eeek... we are up to 12. Maybe lay off the invites for now till I see how many people the restaurant can handle.


# Event
* https://www.zomato.com/melbourne/enelssie-cafe-grill-burnside-heights  
    I will **book** a few days prior when I know the numbers.
* https://www.broadsheet.com.au/melbourne/footscray/shops/t-cavallaro-sons  
    Arguably the best **canolli** in Melbourne from my R&D thus far.

## Menu
* https://www.doordash.com/store/enelssie-caf-and-grill-burnside-heights-1102639
* https://www.zomato.com/melbourne/t-cavallaro-sons-footscray/menu


# Time
* We leave from Jeffs Shed at 1000.  
  Venue is about **34kms** (main plan) or **27kms** (Plan B)  
  So the plan is to get there just at around 1145.
* Or take the **Metro** train to St Albans, Sunbury line  
    (around a 4km ride to venue)
* Or take the train to Caroline Springs, Ballarat/Ararat  
    **VLine** (around a 5km ride to venue)
* Estimate ride time back is about an hour PLUS the stop time at the canolli place.  
    My best guess would be before 3pm to get back to Jeff's Shed.


# Address
Enelssie Cafe and Grill  
102 Tenterfield Dr,  
Burnside Heights  
VIC 3023  
T : +61449775107


# Route (proposed)
* https://www.strava.com/routes/2827379404270709690  
    **Distance** 58.35km (return to Jeffs Shed)  
    Elevation Gain 247m  
    Riding on mostly bike paths.

* Plan B.  
    https://www.strava.com/routes/2824605740751206024  
    **Distance** 51.06km (return to Jeffs Shed)  
    Elevation Gain 195m  
    Is a shorter ride but has more riding on the roads,  
    which may or may not be unpleasant.
    

# Notes:
* The route is littered with train stations.  
  So one could do a **1 way** ride there or back.
* Proposed route is *mostly* bike paths. Am open to any
  suggested tweaks.
* Estimated speed - low 20s.  
  Speed given allocated time - high teens
* There is a **whatsapp** group for this.  
    Please ask to be **added**.


# Disclaimer
* I am of the opinion that amongst all the cuisines  
    from South East Asia, the Filipino cuisine is the  
    worst of the lot.
* I bear **no** responsibility as to whether you will like the food or not.  
    Plus whatever **consequences** of consuming said food.
* Will be **cancelled** if it is raining, or too hot (or cold)


# Checklist: (all optional)
* bike lock
* tyre tubes
* myki
* face mask
* I will check if bike shoes are OK